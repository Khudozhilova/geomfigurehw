public class Circle extends GeomFigure{
    private double radius;
    private double center_x;
    private double center_y;
    private String color;

    public double getCenterX () {
        return center_x;
    }
    public double getCenterY () { return center_y; }
    public double getRadius () { return radius; }
    public String getColor () { return color; }

    public void setCenterX(double val) { center_x = val; }
    public void setCenterY(double val) { center_y = val; }
    public void setRadius(double val) { radius = val; }
    public void setColor(String col) { color = col; }
    public Circle ( double r, double x, double y, String c) {
        radius = r;
        center_x = x;
        center_y = y;
        color = c;
    }
    public Circle (double radius) {
        this(radius, 0, 0, "");
    }
    public Circle () {
        this(0, 0, 0, "");
    }

    public String colorCircle(String new_color) {
        String old_color = this.color;
        this.color = new_color;
        return old_color;
    }
    @Override
    public double GetPerimeter() {
        return 2 * Math.PI * this.radius;
    }

    @Override
    public double GetArea() {
        return Math.PI * this.radius * this.radius;
    }
}
