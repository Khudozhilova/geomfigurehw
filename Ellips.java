public class Ellips extends GeomFigure {

    private double radius_min;
    private double radius_max;
    private double center_x;
    private double center_y;
    private String color;

    public double getCenterX () {
        return center_x;
    }
    public double getCenterY () { return center_y; }
    public double getRadiusMin () { return radius_min; }
    public double getRadiusMax () { return radius_max; }
    public String getColor () { return color; }

    public void setCenterX(double val) { center_x = val; }
    public void setCenterY(double val) {  center_y = val; }
    public void setRadiusMin(double val) { radius_min = val; }
    public void setRadiusMax(double val) { radius_max = val; }
    public void setColor(String col) { color = col; }
    public Ellips(double r_min, double r_max, double x, double y, String c) {
        radius_min = r_min;
        radius_max = r_max;
        center_x = x;
        center_y = y;
        color = c;
    }
    public Ellips(double radius_min, double radius_max) {
        this(radius_min, radius_max, 0, 0, "");
    }
    public Ellips() {
        this(0, 0, 0, 0, "");
    }
    @Override
    public double GetPerimeter() {
        return 2 * Math.PI * (Math.sqrt(((2 * radius_max) * (2 * radius_max) + (2 * radius_min) * (2 * radius_min))/8));
    }

    @Override
    public double GetArea() {
        return Math.PI * radius_min * radius_max;
    }

    public String colorElips(String new_color) {
        String old_color = this.color;
        this.color = new_color;
        return old_color;
    }
}
