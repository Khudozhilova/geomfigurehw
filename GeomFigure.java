public abstract class GeomFigure {

    public abstract double GetPerimeter();

    public abstract double GetArea();

}
