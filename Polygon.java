public abstract class Polygon extends GeomFigure implements WithAngles{

    protected double[][] coordinates;

    public Polygon(double[][] coord){
        this.coordinates = new double[coord.length][];
        for(int i = 0; i < coord.length; i++)
            this.coordinates[i] = coord[i].clone();
    }

    private Polygon() {}

    public int getVerticesCount(){
        return coordinates[0].length;
    }
    public double getDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt((x2-x1)*(x2-x1) + (y2-y1)*(y2-y1));
    }


    @Override
    public double[][] printCoordinates() {
        return new double[0][];
    }

    @Override
    public double GetPerimeter() {
        double p = 0.0;
        int n = getVerticesCount();
        for (int i = 1; i < n; i++) {
            p += getDistance(this.coordinates[0][i], this.coordinates[1][i], this.coordinates[0][i-1], this.coordinates[1][i-1]);
        }
        p += getDistance(this.coordinates[0][n-1], this.coordinates[1][n-1], this.coordinates[0][0], this.coordinates[1][0]);
        return p;
    }
}
