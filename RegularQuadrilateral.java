public interface RegularQuadrilateral {
    boolean ParallelSide();
    boolean RightSide();
}
