public abstract class Rhombus extends Polygon implements RegularQuadrilateral {

    private double a;
    private double b;
    private double c;
    private double d;

    private double diag_min;
    private double diag_max;


    public Rhombus(double[][] coord) {
        super(coord);
    }

    @Override
    public boolean ParallelSide() {
        double x1 = this.coordinates[0][0];
        double y1 = this.coordinates[1][0];
        double x2 = this.coordinates[0][1];
        double y2 = this.coordinates[1][1];
        double x3 = this.coordinates[0][2];
        double y3 = this.coordinates[1][2];
        double x4 = this.coordinates[0][3];
        double y4 = this.coordinates[1][3];

        boolean res = false;

        if (x2 - x1 == 0 && x4 - x3 == 0) res = true;
        else if ((y2-y1) / (x2 - x1) == (y4 - y3) / (x4 - x3)) res = true;
        else return false;

        if (x4 - x1 == 0 && x3 - x2 == 0) res = true;
        else if ((y4 - y1) / (x4 - x1) == (y3 - y2) / (x3 - x2)) res = true;
        else return false;

        return res;
    }

    @Override
    public boolean RightSide() {
        return a == b && a == c && a == d;
    }

    @Override
    public double GetArea(){
        return 0.5 * diag_max * diag_min;
    }

}
